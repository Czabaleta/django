from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serialize import UserSerialize
# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerialize
