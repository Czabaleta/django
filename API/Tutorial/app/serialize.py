from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserSerialize(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'username',
            'last_name',
            'email',
            'password',

        )
